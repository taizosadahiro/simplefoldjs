// ページの読み込みを待つ
window.addEventListener('load', init);


class Panel extends THREE.Group{

    constructor(mvseq){
	super();

	let geometry = new THREE.PlaneGeometry(200, 200);
	geometry.center();
	let material = new THREE.MeshToonMaterial({color:0xFFFFFF});
	const plane = new THREE.Mesh(geometry, material);
	this.add(plane);
	
	material = new THREE.MeshToonMaterial({color:0x00FF00});
	const plane2 = new THREE.Mesh(geometry, material);
	plane2.position.set(0,0,0);
	plane2.rotation.y = 3.14159;
	this.add(plane2);


	let vertices = [new THREE.Vector3( 100, -100, 0),
			new THREE.Vector3( 100, 100, 0),
			new THREE.Vector3( -100, 100, 0),
			new THREE.Vector3( -100, -100, 0)
		       ];
	let edgecenters = [new THREE.Vector3(100, 0, 0),
			   new THREE.Vector3(0, 100, 0),
			   new THREE.Vector3(-100, 0, 0),
			   new THREE.Vector3(0, -100, 0)];
	let colors = [{color:0x0000FF}, {color:0xFF0000}];
	for (let i = 0; i < 4; i++){
	    if (mvseq[i]==-1){
		continue;
	    }else{
		if (i%2==0){
		    geometry = new THREE.PlaneGeometry(5, 200);
		}
		else{
		    geometry = new THREE.PlaneGeometry(200, 5);
		}
		material = new THREE.MeshToonMaterial( colors[mvseq[i]] );
		let line = new THREE.Mesh(geometry, material);
		line.translateOnAxis(edgecenters[i],1);
		this.add(line);
	    }
	}
	
    }
}

	
function init() {

    // サイズを指定
    const width = 960;
    const height = 800;

    // レンダラーを作成
    const renderer = new THREE.WebGLRenderer({
        canvas: document.querySelector('#myCanvas'),
	antialias: true
    });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(width, height);

    // シーンを作成
    const scene = new THREE.Scene();

    // カメラを作成
    const camera = new THREE.PerspectiveCamera(45, width / height);
    camera.position.set(0, 0, +1500);

    // パネルを作成
    //let pattern = [[6,4],[6,7]];
    let pattern = [[4,1,3],[6,5,7],[6,7,2]];
    let m = pattern.length+1;
    let n = pattern[0].length+1;

    let mvseqs=[];
    for (let x = 0; x < m; x++){
	mvseqs.push([]);
	for (let y = 0; y < n; y++){
	    mvseqs[x].push([]);
	    for (let z = 0; z < 4; z++){
		mvseqs[x][y].push(-1);
	    }
	}
    }
    for (let i = 0; i < pattern.length; i++){
	for (let j = 0; j < pattern[0].length; j++){
	    if (pattern[i][j]==0){
		mvseqs[i][j][0] = 0;
		mvseqs[i][j][3] = 0;
		mvseqs[i][j+1][2] = 0;
		mvseqs[i][j+1][3] = 1;
		mvseqs[i+1][j][0] = 0;
		mvseqs[i+1][j][1] = 0;
		mvseqs[i+1][j+1][1] = 1;
		mvseqs[i+1][j+1][2] = 0;
	    }
	    else if (pattern[i][j]==1){
		mvseqs[i][j][0] = 1;
		mvseqs[i][j][3] = 0;
		mvseqs[i][j+1][2] = 1;
		mvseqs[i][j+1][3] = 0;
		mvseqs[i+1][j][0] = 0;
		mvseqs[i+1][j][1] = 0;
		mvseqs[i+1][j+1][1] = 0;
		mvseqs[i+1][j+1][2] = 0;
	    }
	    else if (pattern[i][j]==2){
		mvseqs[i][j][0] = 0;
		mvseqs[i][j][3] = 1;
		mvseqs[i][j+1][2] = 0;
		mvseqs[i][j+1][3] = 0;
		mvseqs[i+1][j][0] = 0;
		mvseqs[i+1][j][1] = 1;
		mvseqs[i+1][j+1][1] = 0;
		mvseqs[i+1][j+1][2] = 0;
	    }
	    else if (pattern[i][j]==3){
		mvseqs[i][j][0] = 0;
		mvseqs[i][j][3] = 0;
		mvseqs[i][j+1][2] = 0;
		mvseqs[i][j+1][3] = 0;
		mvseqs[i+1][j][0] = 1;
		mvseqs[i+1][j][1] = 0;
		mvseqs[i+1][j+1][1] = 0;
		mvseqs[i+1][j+1][2] = 1;
	    }
	    else if (pattern[i][j]==4){
		mvseqs[i][j][0] = 1;
		mvseqs[i][j][3] = 1;
		mvseqs[i][j+1][2] = 1;
		mvseqs[i][j+1][3] = 0;
		mvseqs[i+1][j][0] = 1;
		mvseqs[i+1][j][1] = 1;
		mvseqs[i+1][j+1][1] = 0;
		mvseqs[i+1][j+1][2] = 1;
	    }
	    else if (pattern[i][j]==5){
		mvseqs[i][j][0] = 0;
		mvseqs[i][j][3] = 1;
		mvseqs[i][j+1][2] = 0;
		mvseqs[i][j+1][3] = 1;
		mvseqs[i+1][j][0] = 1;
		mvseqs[i+1][j][1] = 1;
		mvseqs[i+1][j+1][1] = 1;
		mvseqs[i+1][j+1][2] = 1;
	    }
	    else if (pattern[i][j]==6){
		mvseqs[i][j][0] = 1;
		mvseqs[i][j][3] = 0;
		mvseqs[i][j+1][2] = 1;
		mvseqs[i][j+1][3] = 1;
		mvseqs[i+1][j][0] = 1;
		mvseqs[i+1][j][1] = 0;
		mvseqs[i+1][j+1][1] = 1;
		mvseqs[i+1][j+1][2] = 1;
	    }
	    else if (pattern[i][j]==7){
		mvseqs[i][j][0] = 1;
		mvseqs[i][j][3] = 1;
		mvseqs[i][j+1][2] = 1;
		mvseqs[i][j+1][3] = 1;
		mvseqs[i+1][j][0] = 0;
		mvseqs[i+1][j][1] = 1;
		mvseqs[i+1][j+1][1] = 1;
		mvseqs[i+1][j+1][2] = 0;
	    }
		
	}
    }

    let panel = [];
    for (let x = 0; x < m; x++){
	panel.push([]);
	for (let y = 0; y < n; y++){
	    panel[x].push(new Panel(mvseqs[x][y]));
	    panel[x][y].translateOnAxis(new THREE.Vector3(1,0,0),200*(y-n/2));
	    panel[x][y].translateOnAxis(new THREE.Vector3(0,1,0),-200*(x-m/2));
	    scene.add(panel[x][y]);
	}
    }

    let groups = [
	[[0,0],[1,0],[2,0],[3,0]],
	[[0,1],[0,0],[0,2],[0,3]],
	[[1,1],[1,0],[0,1],[0,0],[1,2],[0,2],[1,3],[0,3]],
	[[2,3],[1,3],[0,3],[3,3]],
	[[3,1],[3,0],[3,2],[3,3]]
    ];
    let	mvseq=["M","V","M","V","M"];
    let	rcseq=["C","R","R","C","R"];
    let	numseq=[0,0,0,1,0];
    
    // 平行光源
    const directionalLight = new THREE.DirectionalLight(0xFFFFFF);
    directionalLight.position.set(1, 1, 1);
    // シーンに追加
    scene.add(directionalLight);

    renderer.render(scene, camera); // レンダリング


    let M = m;
    let N = n;
    let s = 0;
    let t = 0;
    
    function moveOn(stage){

	console.log("stage " + stage + " " + rcseq[stage]);
	if (stage >= groups.length) return;
	for (let i = 0; i < m; i++){
	    for (let j = 0; j < n; j++){
		x[i][j] = -Math.round(panel[i][j].position.y/200)+m/2;
	    }
	}
	for (let i = 0; i < m; i++){
	    for (let j = 0; j < n; j++){
		y[i][j] = Math.round(panel[i][j].position.x/200)+n/2;
	    }
	}
	if (stage == 0) return;
	if (rcseq[stage-1] == "C"){
	    let c = numseq[stage-1];
	    if (c+1 > N-(c+1)){
		N = c+1;
	    }
	    else{
		N -= (c+1);
		t += (c+1);
	    }
	}
	else{
	    let r = numseq[stage-1];
	    if (r+1 > N-(r+1)){
		M = r+1;
	    }
	    else{
		M -= (r+1);
		s += (r+1);
	    }
	}
	console.log("("+M + "," + N + ")" + " : (" + s + "," + t + ")");
	console.log(x,y);
    }

    let rect1 = new THREE.Group();
    let rect2 = new THREE.Group();

    let invspeed = 200;
    let count = 0;
    let stage = 0;

    let x = [];
    for (let i = 0; i < m; i++){
	x.push([]);
	for (let j = 0; j < n; j++){
	    x[i].push(i);
	}
    }
    let y = [];
    for (let i = 0; i < m; i++){
	y.push([]);
	for (let j = 0; j < n; j++){
	    y[i].push(j);
	}
    }

    
    moveOn(0);
    tick();
    
    // 毎フレーム時に実行されるループイベントです
    function tick() {
	if (stage < groups.length){
	    if (count == invspeed){
		count = 0;
		stage++;
		moveOn(stage);
		console.log(panel[0][0].position);
	    }
	    else{
		let theta = count*Math.PI/invspeed;
		for (let ij of groups[stage]){
		    if (rcseq[stage] == "C"){
			let c = numseq[stage];
			let smaller = (c+1 < N-(c+1));
			let valley = (mvseq[stage] == "V");
			if ((smaller && valley) || !(smaller || valley)){
			    panel[ij[0]][ij[1]].position.x= (t+c-n/2+1/2)*200 - (t+c-y[ij[0]][ij[1]]+1/2)*Math.cos(theta)*200
			    panel[ij[0]][ij[1]].position.z=Math.sin(theta)*(100+200*(t+c-y[ij[0]][ij[1]]));
			    panel[ij[0]][ij[1]].rotation.y = theta;
			}
			else{
			    panel[ij[0]][ij[1]].position.x= (t+c-n/2+1/2)*200 - (t+c-y[ij[0]][ij[1]]+1/2)*Math.cos(theta)*200
			    panel[ij[0]][ij[1]].position.z=Math.sin(theta)*(100 - 200*(t + c + 1 - y[ij[0]][ij[1]]));
			    panel[ij[0]][ij[1]].rotation.y = -theta;
			}
		    }
		    else{
			let r = numseq[stage];
			let smaller = (r+1 < M-(r+1));
			let valley = (mvseq[stage] == "V");
			if ((smaller && valley) || !(smaller || valley)){
			    panel[ij[0]][ij[1]].position.y= -(s+r-m/2+1/2)*200 + (s + r - x[ij[0]][ij[1]]+1/2)*Math.cos(theta)*200
			    panel[ij[0]][ij[1]].position.z=Math.sin(theta)*(100+200*(s + r -x[ij[0]][ij[1]]));
			    panel[ij[0]][ij[1]].rotation.x = theta;
			}
			else{
			    panel[ij[0]][ij[1]].position.y= -(s+r-m/2+1/2)*200 + (s + r - x[ij[0]][ij[1]]+1/2)*Math.cos(theta)*200
			    panel[ij[0]][ij[1]].position.z=Math.sin(theta)*(100 - 200*(s + r + 1 - x[ij[0]][ij[1]]));
			    panel[ij[0]][ij[1]].rotation.x = -theta;
			}
		    }
		}
	    }
	    count++;
	}
        renderer.render(scene, camera); // レンダリング
        requestAnimationFrame(tick);
    }
}
